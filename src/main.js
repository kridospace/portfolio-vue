import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import VueRouter from 'vue-router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueSimpleMarkdown from 'vue-simple-markdown'
import 'vue-simple-markdown/dist/vue-simple-markdown.css'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Contact from './views/Contact.vue'
import Project from './views/Project.vue'
import Projects from './views/Projects.vue'

Vue.use(VueRouter)
Vue.use(axios)
Vue.use(BootstrapVue)
Vue.use(VueSimpleMarkdown)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

const routes = [
  {
    path: '/',
    name: "Home",
    component: Home
  },
  {
    path: '/about',
    name: "About",
    component: About
  },
  {
    path: '/contact',
    name: "Contact",
    component: Contact
  },
  {
    path: '/project/:slug',
    name: "Project",
    component: Project
  },
  {
    path: '/projects',
    name: "Projects",
    component: Projects
  }
];

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
